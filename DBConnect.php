<?php
/**
 * Created by PhpStorm.
 * User: marko
 * Date: 10/7/17
 * Time: 2:43 PM
 */

class DBConnect
{
    private static $instance;
    private $pdo = null;
    private $pdoError = null;

    private function __construct() {
        $config = $this->getConfig();

        $DB_HOST = 'localhost';
        $DB_USER = $config['user'];
        $DB_PASS = $config['password'];
        $DB_DATABASE = $config['database'];

        try {
            $this->pdo = new PDO("mysql:host=$DB_HOST;dbname=$DB_DATABASE", $DB_USER, $DB_PASS);
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch(PDOException $e) {
            $this->pdoError = $e;
        }
    }

    private function getConfig() : array {
        $returnArray = [];

        $config = file('config');

        // remove comments from config
        $arraysToBeRemoved = [];
        for ($i=0; $i<count($config); $i++) {
            $line = trim($config[$i]);

            if ($line[0] === '#') {
                array_push($arraysToBeRemoved, $i);
            }
        }

        for ($i=0; $i<count($arraysToBeRemoved); $i++) {
            unset($config[$arraysToBeRemoved[$i]]);
        }

        // populate associative array
        for ($i=0; $i<=count($config); $i++) {
            $line = trim($config[$i]);

            $elements = explode(':', $line);

            switch (trim($elements[0])) {
                case 'user':
                    $returnArray['user'] = trim($elements[1]);
                    break;
                case 'password':
                    $returnArray['password'] = trim($elements[1]);
                    break;
                case 'database':
                    $returnArray['database'] = trim($elements[1]);
                    break;
            }
        }

        return $returnArray;
    }

    /**
     * @return PDO
     */
    static public function getPdo(): PDO
    {
        if (!isset(self::$instance)) self::$instance = new self();
        return self::$instance->pdo;
    }

    /**
     * @return Exception|PDOException
     */
    static public function getPdoError(): PDOException
    {
        if (!isset(self::$instance)) self::$instance = new self();
        return self::$instance->pdoError;
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: marko
 * Date: 10/7/17
 * Time: 1:27 PM
 */

Router::add('/', function () {
    echo "home";
});

Router::add('/home', function () {
    echo "home";
});

Router::add('/login', function () {
    if (count($_GET)>0) {
        $user = \Model\User::getUser($_GET['email'], $_GET['password']);
//        var_dump($user);

        if ($user) {
            $_SESSION['email'] = $user.getEmail();
        }
    }

    include_once ('Templates/login_tmp.php');
});

Router::add('/register', function () {
    if (count($_GET)>0) {
        $email = $_GET['email'];
        $username = $_GET['username'];
        $password = $_GET['password'];

        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            echo "<h3>Incorrect email form</h3>";
        } else {
            $obj = \Model\User::createUser($username, $password, $email);
        }
    }

    include_once ('Templates/register_tmp.php');
});

Router::add('/search', function () {
    echo "search";
});

Router::add('/logout', function () {
    echo "logout";

//    unset($_SESSION['user'];
    unset($_SESSION);
    session_destroy();
});
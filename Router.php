<?php
/**
 * Created by PhpStorm.
 * User: marko
 * Date: 10/7/17
 * Time: 1:14 PM
 */

class Router
{
    static private $_url;
    static private $routes;

    static function run() {
        self::$_url = $_SERVER['REQUEST_URI'];
        self::_handleRequest();
    }

    static private function _handleRequest() {
        $request = explode('/', self::$_url);
        $request = $request[1];

        $request2 = explode('?', $request);
        $request2 = $request2[0];

        $route = function (){};

        if (isset(self::$routes['/'.$request])) {
            $route = self::$routes['/'.$request];
        }

        if (isset(self::$routes['/'.$request2])) {
            $route = self::$routes['/'.$request2];
        }

        $route();
    }

    static function add($route, $callback) {
        self::$routes[$route] = $callback;
    }
}
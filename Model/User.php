<?php
/**
 * Created by PhpStorm.
 * User: marko
 * Date: 10/7/17
 * Time: 2:18 PM
 */

namespace Model;

use \DBConnect as DB;

class User
{
    private $id;
    private $name;
    private $email;

    function __construct($id, $name, $email) {
        $this->id = $id;
        $this->name = $name;
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    static function getUser($email, $password) {
        if(!self::checkUser($email)) return null;

        try {
            $stmt = DB::getPdo()->prepare('SELECT * FROM users WHERE email = :email AND password = sha1(:password)');
            $stmt->bindValue(':email', $email, \PDO::PARAM_STR);
            $stmt->bindValue(':password', $password, \PDO::PARAM_STR);
            $stmt->execute();

            $row = $stmt->fetchAll(\PDO::FETCH_ASSOC);

            return new User($row[0]['id'], $row[0]['name'], $row[0]['email']);
        } catch(\PDOException $e) {}

        return null;
    }

    static function createUser($user, $pwd, $email) {
        if(self::checkUser($email)) return false;

        try {
            $stmt = DB::getPdo()->prepare('INSERT INTO users(`name`, `password`, `email`) VALUE(?, sha1(?), ?)');
            return $stmt->execute(array($user, $pwd, $email));
        } catch(\PDOException $e) {}

        return false;
    }

    static function checkUser($email): bool {
        try {
            $stmt = DB::getPdo()->prepare('SELECT * FROM users WHERE email = :email');
            $stmt->bindValue(':email', $email, \PDO::PARAM_STR);
            $stmt->execute();

            $row = $stmt->fetchAll(\PDO::FETCH_ASSOC);

            // ako nema rezultata
            if(!count($row))
                return false;
            return true;
        } catch(\PDOException $e) {
            var_dump($e->errorInfo);
            return false;
        }
    }
}
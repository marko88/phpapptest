CREATE USER 'test_user123456789' IDENTIFIED WITH mysql_native_password;
SET PASSWORD FOR 'test_user123456789' = PASSWORD('password');

CREATE DATABASE IF NOT EXISTS test_app;

USE test_app;

CREATE TABLE IF NOT EXISTS users
(
    id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name CHAR(60) NOT NULL,
    email CHAR(80) NOT NULL,
    password CHAR(40) NOT NULL
);

GRANT SELECT, INSERT, UPDATE, DELETE
ON test_app.*
TO test_user123456789 IDENTIFIED BY 'password';

INSERT INTO users VALUE (0, 'admin', 'admin@example.com', sha1('admin'));
<?php
/**
 * Created by PhpStorm.
 * User: marko
 * Date: 10/7/17
 * Time: 12:59 PM
 */

session_start();

/**
 * Setting up autoloader
 */

// Your custom class dir
define('CLASS_DIR', '');

// Add your class dir to include path
set_include_path(get_include_path().PATH_SEPARATOR.CLASS_DIR);

// You can use this trick to make autoloader look for commonly used "My.class.php" type filenames
spl_autoload_extensions('.php');

// Use default autoload implementation
spl_autoload_register();

/**
 * Calling routes
 */

require_once('routes.php');

Router::run();

runvar_dump($GLOBALS);

function is_registred() {

};